/* Read me
    Install Guide
    Step1:Install ESP32
     -ESP32 link in preferences(Arduino IDE => File  => preference) https://dl.espressif.com/dl/package_esp32_index.json (ESP32 V 1.0.2 Engin) make sure!!!
     -Download Board ESP32 from Tools => Board: => Boards Manager => key Input "esp32" => Click Install 
    Step2:Use libraly and Dowload From this link
     -HX711 =>     https://github.com/bogde/HX711
     -Ping test => https://github.com/marian-craciunescu/ESP32Ping 
     -NTPClient => https://github.com/arduino-libraries/NTPClient
     -ESP32 BLE => https://github.com/nkolban/ESP32_BLE_Arduino
     -Firebase  => https://github.com/artronshop/IOXhop_FirebaseESP32/blob/master/IOXhop_FirebaseESP32.h
                   Note* Firebase lib use ArduinoJson V5.13.3 =>https://github.com/bblanchon/ArduinoJson/releases/tag/v5.13.3
    Step3:Setup Tools
     Arduino IDE Program => Tools => Partition Scheme => No OTA (2MB APP/2B FATFS)
    Step4: Done and Enjoy.
     - Use with App Smart Cushion
   ******************************************
    Dev: Smart Cushion Project 2021 
         Naresuan University-Electrical Engineering
         Update 23/02/2023
   ******************************************      
*/

//EEP-ROM Libraly 
  #include <EEPROM.h>
  #define EEPROM_SIZE 128
//Wifi && Wifi-ping Libraly 
  #include <WiFi.h>
  #include "time.h"
  #include <ESP32Ping.h>
      const char* remote_host = "www.google.com";//test ping @google 
//Bletooth Libraly 
  #include <BLEDevice.h>
  #include <BLEServer.h>
  #include <BLEUtils.h>
  #include <BLE2902.h>
      #define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
      #define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
//HX711 Libraly 
  #include <HX711.h>
//firebase esp32 Libraly 
  #include <IOXhop_FirebaseESP32.h>
      #define FIREBASE_HOST "https://smartcushion-4b3db-default-rtdb.asia-southeast1.firebasedatabase.app/"
      #define FIREBASE_AUTH "3WnOwWmU935lpqimuWGKPTfhBHi01xIQQlUq6nwL"
//NTPC-time Libraly 
#include <NTPClient.h>
      const char* ntpServer = "asia.pool.ntp.org";
      const int  gmtOffset_sec = 3.5*3600;
      const int   daylightOffset_sec = 3.5*3600;

// define pin esp32
  #define LOADCELL01_DOUT_PIN 22
  #define LOADCELL01_SCK_PIN 23
  
  #define LOADCELL02_DOUT_PIN 18
  #define LOADCELL02_SCK_PIN 19
  
  #define LOADCELL03_DOUT_PIN 16
  #define LOADCELL03_SCK_PIN 17
  
  #define LOADCELL04_DOUT_PIN 14
  #define LOADCELL04_SCK_PIN 27
  #define motor 5
  #define LED_ST 25

// int variable
  int silenceMode=0;
  int ShouldBeUpTime;
  int AlarmCheck=0;
  int OverTimeAdd=0;
  int OverAlready=5;

// bool variable
  bool ST;
  bool sit;
  bool FullTime=false;
  bool SentAlarm=false;
bool ClearRuntime=false;
bool SitAlready =false;
bool backsit=false;
bool SitToUp=false;

// long variable
long timesitApp=0;
long SitTime;
long PicSitTime;
long RunTime;
long UpTime;
long PicUpTime;

//String variable
  String SMART_NAME;//รับชื่อ user name
  String HR;
  String MIN;
  String SEC;
  String Dd;
  String Mm;
  String Yy;
  String Dsent;
  String timesent;
  
//float variable1
float A;
float B;
float C;
float D;
    float a;
    float b;
    float c;
    float d;

float Weightsit=10.0; // ตั้งค่าน้ำหนักขั้นต่ำ

HX711 scale01;
HX711 scale02;
HX711 scale03;
HX711 scale04;

// bluetooth 
BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;

  const int ledPin = 25;
  const int modeAddr = 33;
  const int wifiAddr = 10;
int modeIdx;

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      BLEDevice::startAdvertising();
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};
class MyCallbacks: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic){
    std::string value = pCharacteristic->getValue();

    if(value.length() > 0){
      Serial.print("Value : ");
      Serial.println(value.c_str()); 
      writeString(wifiAddr, value.c_str());
 ESP.restart();
    }
  }
  
void writeString(int add, String data){
    int _size = data.length();
    for(int i=0; i<_size; i++){
      EEPROM.write(add+i, data[i]);
    }
    EEPROM.write(add+_size, '\0');
    EEPROM.commit();
  }
};

//...............SETUP...................//

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_ST, OUTPUT);

  if(!EEPROM.begin(EEPROM_SIZE)){
    delay(1000);
  }
  Serial.printf("EEPROM_SIZE");
  Serial.println(EEPROM_SIZE);
  
  modeIdx = EEPROM.read(modeAddr);
  Serial.print("modeIdx : ");
  Serial.println(modeIdx);
  Serial.print("modeADD : ");
  Serial.println(modeAddr);
  EEPROM.write(modeAddr, modeIdx !=0 ? 0 : 1);
  EEPROM.commit();
  
  if(modeIdx != 0){
    //BLE MODE
    digitalWrite(LED_ST, true);
    Serial.println("BLE MODE");
    
    bleTask();
    ST=false;    
  }else{
    //WIFI MODE
    digitalWrite(LED_ST, false);
    Serial.println("WIFI MODE");
    wifiTask();
    
  
    
Serial.println("Initializing the scale");
  scale01.begin(LOADCELL01_DOUT_PIN, LOADCELL01_SCK_PIN);
  scale02.begin(LOADCELL02_DOUT_PIN, LOADCELL02_SCK_PIN);
  scale03.begin(LOADCELL03_DOUT_PIN, LOADCELL03_SCK_PIN);
  scale04.begin(LOADCELL04_DOUT_PIN, LOADCELL04_SCK_PIN);
  
  scale01.set_scale(2280.f);
  scale01.tare();
  scale02.set_scale(2280.f); 
  scale02.tare();
  scale03.set_scale(2280.f);
  scale03.tare();
  scale04.set_scale(2280.f);
  scale04.tare(); 
Serial.println("Readings:");
Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
Firebase.set(SMART_NAME+"/""Value/""stime",0);  //ส่งเวลานั่งเป็นค่าเริ่มต้น (0)
// Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();
  digitalWrite(LED_ST,1);
  delay(300);
    digitalWrite(LED_ST,0);
  delay(300);
    digitalWrite(LED_ST,1);
  delay(300);
    digitalWrite(LED_ST,0);
  delay(300);
  }
  pinMode(motor,OUTPUT);
}

//เรียกใช้งาน bletooth
  void bleTask(){

        // Create the BLE Device
        BLEDevice::init("Smart Cushion");   
        // Create the BLE Server
        pServer = BLEDevice::createServer();
        pServer->setCallbacks(new MyServerCallbacks());
     
        // Create the BLE Service
        BLEService *pService = pServer->createService(SERVICE_UUID);
        // Create a BLE Characteristic
        pCharacteristic = pService->createCharacteristic(
                            CHARACTERISTIC_UUID,
                            BLECharacteristic::PROPERTY_READ   |
                            BLECharacteristic::PROPERTY_WRITE  |
                            BLECharacteristic::PROPERTY_NOTIFY |
                            BLECharacteristic::PROPERTY_INDICATE
                          );
        pCharacteristic->setCallbacks(new MyCallbacks());
        pCharacteristic->addDescriptor(new BLE2902());
        // Start the service
        pService->start();
        // Start advertising
        BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
        pAdvertising->addServiceUUID(SERVICE_UUID);
        pAdvertising->setScanResponse(false);
        pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
        BLEDevice::startAdvertising();
        Serial.println("Waiting a client connection to notify...");

   }

//สลับใช้งาน wifi เมื่อเชื่อม bletooth เสร็จ
    void wifiTask() {
          String receivedData;
          receivedData = read_String(wifiAddr);
        
          if(receivedData.length() > 0){
            String wifiName = getValue(receivedData, ',', 0);
            String wifiPassword = getValue(receivedData, ',', 1);
            SMART_NAME= getValue(receivedData, ',', 2); //ชื่อผู้ใช้ที่ได้รับจาก bletooth
            
            if(wifiName.length() > 0 && wifiPassword.length() > 0){
              Serial.print("WifiName : ");
              Serial.println(wifiName);
        
              Serial.print("wifiPassword : ");
              Serial.println(wifiPassword);
        
              WiFi.begin(wifiName.c_str(), wifiPassword.c_str());
              Serial.print("Connecting to Wifi");
              while(WiFi.status() != WL_CONNECTED){
                Serial.print(".");
                delay(300);
              }
              Serial.println();
              Serial.print("Connected with IP: ");
                ST=true;
              Serial.println(WiFi.localIP());
              Serial.print("Ping Host: ");
              Serial.println(remote_host);
              if(Ping.ping(remote_host)){
                Serial.println("Success!!");
              }else{
                Serial.println("ERROR!!");
              }
            }
          }
        }
String read_String(int add){
  char data[100];
  int len = 0;
  unsigned char k;
  k = EEPROM.read(add);
  while(k != '\0' && len< 500){
    k = EEPROM.read(add+len);
    data[len] = k;
    len++;
  }
  data[len] = '\0';
  return String(data);
}
String getValue(String data, char separator, int index){
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found <=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
      found++;
      strIndex[0] = strIndex[1]+1;
      strIndex[1] = (i==maxIndex) ? i+1 : i;
    }
  }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

//.............loop.................//
void loop(){
  //เมื่อโปรแกรมเข้าสู่ Wifi Mode
  if(ST==true)
  {
     read_HX711(); // อ่านค่าน้ำหนักจาก HX711
     printLocalTime(); // รับค่าเวลาปัจจุบัน จาก NTP Server
     //เมื่อค่าน้ำหนักรวมมากกว่า Weightsit ที่ตั้งไว้
      if((a+b+c+d)>=Weightsit){
        get_dataFirebase();//we have timesitApp paramiter  
        sit = true;
       } 
     //เมื่อค่าน้ำหนักรวมน้อยกว่า Weightsit ที่ตั้งไว้  
      else{
        sit = false;
      } 
      //ผลของเงื่อนำไขการทำงานด้านบน => 
            //กรณีนั่งบนเบาะรองนั่ง
              if(sit==true && FullTime==false){  
                // ส่งข้อมูลน้ำหนัก
                
                    if(SitAlready == false){
                      SitTime=millis()-RunTime;
                      }
                    else if(SitAlready == true){
                      SitTime=PicSitTime+(millis()-RunTime);  
                      }  
                      sentDataToFirebase();
                              // เมื่อนั่งครบตามเวลาแล้ว
                              if(SitTime>=timesitApp){
                                FullTime=true;
                                RunTime=millis();
                              }
                  SitToUp=false;
              }
            // กรณีนั่งครบเวลาไม่ลุกออกไปด้านนอก 
            else if(sit==true && FullTime==true) {
              if(backsit==false){
                      // ส่งค่าเพื่อแจ้งเตือนแอพ Alarm 1 ครั้ง
                                        if(SentAlarm==false){        
                                                  Firebase.set(SMART_NAME+"/""Value/""Alarm",1);
                                                  SentAlarm=true;
                                                  }
                                         // รับค่า Alarm เพื่อเช็ค AlarmCheck       
                                          get_dataAlarm();   
                                         // เข้าสู่เงื่อนไข AlarmCheck และสั่นแจ้งเตือน
                                          if(AlarmCheck==1){
                                              motorVibration();
                                              OverTimeAdd++;
                                              }
                                          else if(AlarmCheck==0){
                                              Firebase.push(SMART_NAME+"/""Value/""Counting""/"+Dsent+"/""CancelAlarm/",timesent);
                                              RunTime=millis();  
                                              FullTime=false;
                                              PicSitTime=0;
                                              SitAlready=false; 
                                              SentAlarm=false;
                                              }
                                          if(OverTimeAdd==OverAlready){
                                              Firebase.push(SMART_NAME+"/""Value/""Counting""/"+Dsent+"/""OverTimeCount/",timesent);
                                           SentAlarm=false;
                                          }  
                          RunTime=millis();                   
                   }
                // ถ้าลุกไม่ครบเวลาแล้วกลับมานั่งให้เบาะสั่น     
                else{
                      motorVibration();
                    }                         
              
            } 

            // กรณีนั่งครบเวลาแล้วต้องลุกออกไปด้านนอก  
            else if(sit==false && FullTime==true) {
              PullUpTime();
              UpTime=millis()-RunTime;
              backsit=true;

              if(SitToUp==false){
                        SitToUp=true;
                        Firebase.set(SMART_NAME+"/""Value/""Alarm",0);
                }
                              // ลุกครบเวลาที่กำหนดแล้ว
                              if(UpTime>=ShouldBeUpTime){
                                  FullTime=false;
                                  Firebase.set(SMART_NAME+"/""Value/""stime",0);    
                                }
              }

            // กรณีนั่งยังไม่ครบเวลา หรือยังไม่ได้นั่ง
             else if(sit==false && FullTime==false) {
               backsit=false;
               OverTimeAdd=0;
                RunTime=millis();
                UpTime=0;
                if(SitToUp==false){
                  PicSitTime=SitTime; 
                }
                else if(SitToUp==true){
                  PicSitTime=0;
                }
                // กรณีนั่งยังไม่ครบเวลา
                 
                  if(PicSitTime != 0){
                    SitAlready=true; 
                    }
                    else if(PicSitTime == 0){
                       SitAlready=false; 
                    }
                    
                }                   
          }
  }


//............All function...........//
void PullUpTime(){
  int FireabaseUpTime=Firebase.getInt(SMART_NAME+"/""Value/""TimeUp");
  ShouldBeUpTime=FireabaseUpTime*1000; // in mili S
  }
  
//ฟังก์ชั่นแสดงเวลาจาก server   
void printLocalTime(){
  time_t rawtime;
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo))
  {
      digitalWrite(LED_ST,1);
  delay(1000);
      digitalWrite(LED_ST,0);
  delay(200);
      digitalWrite(LED_ST,1);
  delay(200);
      digitalWrite(LED_ST,0);
  delay(200);
      digitalWrite(LED_ST,1);
  delay(200);
      digitalWrite(LED_ST,0);
  delay(200);
    Serial.println("Failed to obtain time");
   return;
  }
 Dd= timeinfo.tm_mday;
 Mm = timeinfo.tm_mon + 1;
 Yy= timeinfo.tm_year +1900;

int JKK=Dd.toInt();
int JLL=Mm.toInt();
int SDF=sizeof(JKK);
String Zeroo="0";
 
      if(JKK<10){
          Dd=""+Zeroo+""+Dd+"";
        }
      if(JLL<10){
         Mm=""+Zeroo+""+Mm+"";
        }  
HR=timeinfo.tm_hour;
MIN=timeinfo.tm_min;
SEC= timeinfo.tm_sec;
                
int KHH=HR.toInt();
int KMM=MIN.toInt();                        
int KSS=SEC.toInt();

//สำหรับแนบเวลาที่ส่งไปยัง database
                        if(KHH<10&&KHH!=0){
                            HR=""+Zeroo+""+HR+"";
                          }
                        if(KMM<10&&KMM!=0){
                           MIN=""+Zeroo+""+MIN+"";
                          }  
                           
                        if(KSS<10&&KSS!=0){
                           SEC=""+Zeroo+""+SEC+"";
                        }
                        
                        if(KHH==0){
                           HR="00";
                          }                                                       
                        if(KMM==0){
                           MIN="00";
                          } 
                        if(KSS==0){
                           SEC="00";
                          } 
Dsent=""+Yy+""+Mm+""+Dd+"";
timesent=""+HR+""+MIN+""+SEC+"";
//Serial.println(Dsent+":"+timesent);
}

//ฟังก์ชั่นวัดน้ำหนัก
void read_HX711(){
    A=scale01.get_units(),2;
    B=scale02.get_units(),2;
    C=scale03.get_units(),2;
    D=scale04.get_units(),2;  

    //Calibration Sensor to Kg.
    
            a=(abs(A)/10)*0.453592;
            b=(abs(B)/10)*0.453592;
            c=(abs(C)/10)*0.453592;    
            d=(abs(D)/10)*0.453592;

 
 }
//ฟังก์ชั่นเรียกข้อมูล
  void get_dataFirebase(){
  silenceMode = Firebase.getInt(SMART_NAME+"/""Value/""silenceMode"); // vibration_get=รับค่าการสถานะการแจ้งเตือนจากมือถือ 0 = สั่น 1 = ไม่สั่น
  timesitApp  = Firebase.getInt(SMART_NAME+"/""Value/""AlertTimeInSeconds");
  timesitApp=timesitApp*1000; // in mili S
  }
  
  void get_dataAlarm(){
  AlarmCheck=Firebase.getInt(SMART_NAME+"/""Value/""Alarm");
  }
  
// ฟังก์ชั่น ส่งข้อมูล 
 void sentDataToFirebase(){
          int sec_sent = SitTime/1000;
          Firebase.set(SMART_NAME+"/""Value/""stime",sec_sent);          
          //show realtimA_Sent
           if(SitTime<timesitApp){
          Firebase.set(SMART_NAME+"/""Value/""A", a);
          Firebase.set(SMART_NAME+"/""Value/""B", b);
          Firebase.set(SMART_NAME+"/""Value/""C", c);
          Firebase.set(SMART_NAME+"/""Value/""D", d);
          
          Firebase.push(SMART_NAME+"/""DATE/"+Dsent +"/"+ timesent+"/""A""",a);   //ชื่อผู้ใช้งาน/date/วันส่ง/ เวลาที่ส่ง / ตำแหน่งเซนเซอร์ / ค่าแต่ละตัว (ALL String)
          Firebase.push(SMART_NAME+"/""DATE/"+Dsent +"/"+ timesent+"/""B""",b);
          Firebase.push(SMART_NAME+"/""DATE/"+Dsent +"/"+ timesent+"/""C""",c);
          Firebase.push(SMART_NAME+"/""DATE/"+Dsent +"/"+ timesent+"/""D""",d);
           }
}

// ฟังก์ชั่น สั่น
void motorVibration(){
  if(silenceMode==0 ||silenceMode==NULL)
  {
    digitalWrite(motor,1);
    delay(1200);
    digitalWrite(motor,0);
    delay(500);  
    digitalWrite(motor,1);
    delay(1200);
    digitalWrite(motor,0);
    delay(500);  
    digitalWrite(motor,1);
    delay(1200);
    digitalWrite(motor,0);
    }
  else{
    // Serial.println("silenceMode");
    }
  }
  
